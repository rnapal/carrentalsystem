<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.ibm.bootcamp.casestudy.*,java.util.*"%>  
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<title>ADMIN SIDE</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
<jsp:include page="Navbar.jsp"/>
<!-- TABLE -->
<% List<Car> listCar = CarDao.getAllCar();
request.setAttribute("listCar", listCar);
%>
<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">
  			Add Car
		</button>    
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Model</th>
      <th scope="col">Years</th>
      <th scope="col">Color</th>
      <th scope="col">Plate Number</th>
      <th scope="col">Date Acquired</th>
      <th scope="col">Seating Capacity</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>  
    </tr>
  </thead>
  <tbody>
    <c:forEach var="carList" items="${listCar }">
    <tr>

      <td><c:out value="${carList.model }"></c:out></td>
      <td><c:out value="${carList.year }"></c:out></td>
      <td><c:out value="${carList.color }"></c:out></td>
      <td><c:out value="${carList.plateNumber }"></c:out></td>
      <td><c:out value="${carList.dateAcquired }"></c:out></td>
      <td><c:out value="${carList.seatingCapacity }"></c:out></td>  
      <td><a href="#Edit"class="btn btn-info"><i class="fa fa-edit"></i> Edit</a></td>
      <td><a href="#Delete"class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a></td>     
    </tr>
    </c:forEach>   
  </tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sign in</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="SaveServlet" method="Post">
<label>Car Model</label><br/>
<input type ="text" name="model" /><br/>
<label>Year</label><br/>
<input type ="text" name="year" /><br/>
<label>Color</label><br/>
<input type ="text" name="color" /><br/>
<label>License Plate Number</label><br/>
<input type ="text" name="plateNumber" /><br/>
<label>Date Acquired</label><br/>
<input type ="date" name="dateAcquired" /><br/>
<label>Seating Capacity</label><br/>
<input type ="text" name="seatingCapacity" /><br/><br/>
<input type="submit" value="Add Car"/>
</form>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL -->

</body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>