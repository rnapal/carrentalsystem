package com.ibm.bootcamp.casestudy;

public class LoginUser {
	private int userId;
	private String username;
	private String password;
	private String usertype;

	public LoginUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoginUser(int userId, String username, String password, String usertype) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.usertype = usertype;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

}
