package com.ibm.bootcamp.casestudy;

public class Car {
	private int carId;
	private String model, year, color, plateNumber, dateAcquired, seatingCapacity;

	public Car() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Car(int carId, String model, String year, String color, String plateNumber, String dateAcquired,
			String seatingCapacity) {
		super();
		this.carId = carId;
		this.model = model;
		this.year = year;
		this.color = color;
		this.plateNumber = plateNumber;
		this.dateAcquired = dateAcquired;
		this.seatingCapacity = seatingCapacity;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public String getDateAcquired() {
		return dateAcquired;
	}

	public void setDateAcquired(String dateAcquired) {
		this.dateAcquired = dateAcquired;
	}

	public String getSeatingCapacity() {
		return seatingCapacity;
	}

	public void setSeatingCapacity(String seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}

}
