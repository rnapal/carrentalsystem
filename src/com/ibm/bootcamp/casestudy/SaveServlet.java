package com.ibm.bootcamp.casestudy;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SaveServlet
 */
@WebServlet("/SaveServlet")
public class SaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaveServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text.html");
		PrintWriter pw = response.getWriter();

		String model = request.getParameter("model");
		String year = request.getParameter("year");
		String color = request.getParameter("color");
		String plateNumber = request.getParameter("plateNumber");
		String dateAcquired = request.getParameter("dateAcquired");
		String seatingCapacity = request.getParameter("seatingCapacity");

		try {
			Connection connection = CarDao.getconnection();
			PreparedStatement ps = connection.prepareStatement("SELECT plateNumber FROM car where plateNumber = ?");
			ps.setString(1, plateNumber);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				pw.println("Plate Number already exist!!");
			} else {

				Car car = new Car();
				car.setModel(model);
				car.setYear(year);
				car.setColor(color);
				car.setPlateNumber(plateNumber);
				car.setDateAcquired(dateAcquired);
				car.setSeatingCapacity(seatingCapacity);
				int status = CarDao.save(car);

				if (status > 0) {
					request.setAttribute("alertMsg", "SUCCESS");
					response.sendRedirect("AdminHomepage.jsp");

				} else {
					pw.println("ERROR");
				}

			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
