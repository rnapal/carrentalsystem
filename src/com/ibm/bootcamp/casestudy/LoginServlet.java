package com.ibm.bootcamp.casestudy;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text.html");
		PrintWriter pw = response.getWriter();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		Connection connection = CarDao.getconnection();
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("SELECT * FROM userLogin where username=? and password=?");
			preparedStatement.setString(1, username);
			preparedStatement.setString(2, password);

			ResultSet resultSet = preparedStatement.executeQuery();
			if (username.isEmpty() && password.isEmpty()) {
				// pw.println("<script type=\"text/javascript\">");
				// pw.println("alert('Please complete all fields');");
				pw.println("location='index.jsp';");
				// pw.println("</script>");
			} else {
				if (resultSet.next()) {
					String type = resultSet.getString("userType");
					if ("ADMIN".equals(type)) {
						HttpSession session = request.getSession();
						session.setAttribute("username", username);
						response.sendRedirect("AdminHomepage.jsp");
					} else if ("USER".equals(type)) {
						HttpSession session = request.getSession();
						session.setAttribute("username", username);
						response.sendRedirect("Homepage.jsp");
					}
				} else {
					pw.println("<script type=\"text/javascript\">");
					pw.println("alert('Incorrect Username or Password');");
					pw.println("location='index.jsp';");
					pw.println("</script>");
				}
			}

		} catch (SQLException e) {
			System.out.println(e);
		}
	}

}
