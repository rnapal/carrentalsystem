package com.ibm.bootcamp.casestudy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CarDao {
	public static Connection getconnection() {
		Connection connection = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			connection = DriverManager
					.getConnection("jdbc:mariadb://localhost:3306/CarRentalSystemDb?user=root&password=root");
		} catch (Exception e) {
			System.out.println(e);
		}
		return connection;

	}

	public static int save(Car car) {
		int status = 0;
		try {
			Connection connection = CarDao.getconnection();
			PreparedStatement ps = connection
					.prepareStatement("insert into car() values(0,?,?,?,?,(STR_TO_DATE(?,'%Y-%m-%d')),?)");
			ps.setString(1, car.getModel());
			ps.setString(2, car.getYear());
			ps.setString(3, car.getColor());
			ps.setString(4, car.getPlateNumber());
			ps.setString(5, car.getDateAcquired());
			ps.setString(6, car.getSeatingCapacity());

			status = ps.executeUpdate();
			connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return status;
	}

	public static List<Car> getAllCar() {
		List<Car> listCar = new ArrayList<>();
		try {
			Connection connection = CarDao.getconnection();
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM car");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Car car = new Car();
				car.setModel(rs.getString("model"));
				car.setYear(rs.getString("year"));
				car.setColor(rs.getString("color"));
				car.setPlateNumber(rs.getString("plateNumber"));
				car.setDateAcquired(rs.getString("dateAcquired"));
				car.setSeatingCapacity(rs.getString("seatingCapacity"));
				listCar.add(car);

			}
			connection.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return listCar;
	}

	public static int delete(int carId) {
		int status = 0;
		try {
			Connection connection = CarDao.getconnection();
			PreparedStatement ps = connection.prepareStatement("DELETE from car WHERE carId = ?");
			ps.setInt(1, carId);

			status = ps.executeUpdate();

			connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}
}
